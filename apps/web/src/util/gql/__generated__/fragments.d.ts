import * as Types from '../../../__generated__/graphql-types';

export type UploadCardFieldsFragment = { __typename?: 'UploadRecord', id: string, title?: string | null, thumbnailBlurhash?: string | null, thumbnailUrl?: string | null, channel: { __typename?: 'Channel', id: string, name: string, avatarUrl?: string | null } };
