export const BACKGROUND_QUEUE = 'background';
export const IMPORT_QUEUE = 'import';
export const TRANSCODE_QUEUE = 'transcode';
export const TRANSCRIBE_QUEUE = 'transcribe';
