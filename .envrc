source_up_if_exists
source ./bin/activate-hermit

# https://github.com/cashapp/hermit-packages/issues/309#issuecomment-1506190116
mkdir -p .hermit/node/lib

source_env_if_exists ./.envrc.local

git config --local core.hooksPath $PWD/.githooks
git config --local gpg.ssh.allowedSignersFile $PWD/allowed_signers

# Set environment variables
set -a
: ${COMPOSE_PROJECT_NAME:=letschurch_dev}

: ${HOST_WEB_PORT:=4000}
: ${HOST_GATEWAY_PORT:=4100}
: ${HOST_EXTERNAL_HOOKS_PORT:=4200}
: ${HOST_TEMPORAL_UI_PORT:=4400}
: ${HOST_TEMPORAL_PORT:=4401}
: ${HOST_ES_PORT:=4500}
: ${HOST_KIBANA_PORT:=4501}
: ${HOST_MAILPIT_WEB_PORT:=4600}
: ${HOST_MAILPIT_SMTP_PORT:=4601}
: ${HOST_MAILPIT_SMTPS_PORT:=4602}
: ${HOST_PG_PORT:=5432}

: ${HOST_GATEWAY_URL:=http://localhost:${HOST_GATEWAY_PORT}}

: ${MEDIA_URL:=https://media-preview.letschurch.cloud}

: ${PG_USER:=letschurch}
: ${PG_PASSWORD:=password}
: ${PG_DATABASE:=$PG_USER}
: ${HOST_DATABASE_URL:=postgres://$PG_USER:$PG_PASSWORD@localhost:$HOST_PG_PORT/$PG_DATABASE}

: ${HOST_ELASTICSEARCH_URL:=http://localhost:${HOST_ES_PORT}}

# openssl rand -hex 64
: ${JWT_SECRET:=a5f09624d6106947a62e904ef1f31d880e3c4dde7ac74d67828c197a7f72ec973ca5ccae1210ddad78e590b3734511c0a943fe8ed366640ffb09d5edc9da097a}

: ${ZXCVBN_MINIMUM_SCORE:=3}

: ${CLOUDFLARE_ACCOUNT_ID:=accountid}
: ${CLOUDFLARE_AUTH_EMAIL:=admin@mail.com}
: ${CLOUDFLARE_AUTH_KEY:=apikey}

: ${S3_INGEST_REGION:=us-east-1}
: ${S3_INGEST_ENDPOINT:=s3endpoint}
: ${S3_INGEST_BUCKET:=s3bucket}
: ${S3_INGEST_ACCESS_KEY_ID:=accesskeyid}
: ${S3_INGEST_SECRET_ACCESS_KEY:=secretaccesskey}

: ${S3_PUBLIC_REGION:=us-east-1}
: ${S3_PUBLIC_ENDPOINT:=s3endpoint}
: ${S3_PUBLIC_BUCKET:=s3bucket}
: ${S3_PUBLIC_ACCESS_KEY_ID:=accesskeyid}
: ${S3_PUBLIC_SECRET_ACCESS_KEY:=secretaccesskey}

: ${S3_BACKUP_REGION:=us-east-1}
: ${S3_BACKUP_ENDPOINT:=s3endpoint}
: ${S3_BACKUP_BUCKET:=s3bucket}
: ${S3_BACKUP_ACCESS_KEY_ID:=accesskeyid}
: ${S3_BACKUP_SECRET_ACCESS_KEY:=secretaccesskey}
: ${S3_BACKUP_STORAGE_CLASS:=STANDARD_IA}

: ${RCLONE_CONFIG_LCDEVS3_TYPE:=s3}
: ${RCLONE_CONFIG_LCDEVS3_PROVIDER:=Other}
: ${RCLONE_CONFIG_LCDEVS3_ENDPOINT:=$S3_PUBLIC_ENDPOINT}
: ${RCLONE_CONFIG_LCDEVS3_ACCESS_KEY_ID:=$S3_PUBLIC_ACCESS_KEY_ID}
: ${RCLONE_CONFIG_LCDEVS3_SECRET_ACCESS_KEY:=$S3_PUBLIC_SECRET_ACCESS_KEY}

set +a
# End setting environment variables

path_add NAVI_PATH ./navi
